(function($, window, document) {

    'use strict';

    var app = {
      init: function() {
        var _that = this;
        console.log('Init');

        this.getAllItems().done(function(data) {

          console.log('data', data);
          $('.dash__qty').html(data.length);

          //Create itens
          $.each(data, function(key, value) {
            _that.renderItemsList(value);
          });
        });

      },
      getAllItems: function() {
        var req = $.ajax({
          url: 'http://159.65.105.147:8080/api/items',
          type: 'GET',
          crossDomain: true,
          cache: false
        });
        return req;
      },
      postNewItem: function(item) {
        var req = $.ajax({
          url: 'http://159.65.105.147:8080/api/items',
          type: 'POST',
          data: item,
          crossDomain: true,
          cache: false
        });

        return req;
      },
      submitForm: function(form) {
        let data = {
          "type": $( form ).find('select[name="type"]').val(),
          "state": $( form ).find('select[name="state"]').val(),
          "reason": $( form ).find('select[name="reason"]').val(),
          "description": $( form ).find('textarea[name="description"]').val()
        }

        if(data.type !== 'Selecione o motivo do chamado' && data.state !== 'Selecione o estado' &&  data.reason !== 'Selecione o motivo do chamado' && data.description !== ''){
          app.postNewItem(data).done(function(){
            alert('Cadastro realizado com sucesso!');
            location.reload();
          });

        }else{
          alert('Por favor, verifique se todos os campos estão preenchidos corretamente!');
        }
      },
      getItem: function(item) {
        var req = $.ajax({
          url: 'http://159.65.105.147:8080/api/items/' + item,
          type: 'GET',
          crossDomain: true,
          cache: false
        });

        return req;

      },
      deleteItem: function(item) {
        var req = $.ajax({
          url: 'http://159.65.105.147:8080/api/items/' + item,
          type: 'DELETE',
          crossDomain: true,
          cache: false
        });

        return req;

      },
      renderItemsList: function(item) {
        const monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        let data = new Date(item.date);
        let dia = data.getDate();
        let mes = monthNames[data.getMonth()];
        let ano = data.getFullYear();
        let hora = data.getHours();
        let minuto = data.getMinutes();
        let horario = hora + ':' + minuto;
        data = dia + ' de ' + mes + ' de ' + ano;

        console.log('mes', mes);

        console.log('horario', horario);

        var template = '<li class="list__item" data-item="{{itemId}}">' +
                          '<div class="list__item--col-left">' +
                            '<span class="list__item--date"><small>Data:</small>{{itemDate}}</span>' +
                            '<span class="list__item--hour"><small>Hora:</small>{{itemHour}}</span>' +
                            '<span class="list__item--type"><small>Tipo de chamado:</small>{{itemType}}</span>' +
                          '</div>'+
                          '<div class="list__item--col-right">'+
                            '<div class="list__item--description">'+
                              '<small>Descrição:</small><p>{{itemDescription}}</p>' +
                            '</div>' +
                          '</div>' +
                        '</li>';
        template = template.replace(/\{\{itemId\}\}/g, item._id)
                            .replace(/\{\{itemDate\}\}/g, data)
                            .replace(/\{\{itemHour\}\}/g, horario)
                            .replace(/\{\{itemType\}\}/g, item.type)
                            .replace(/\{\{itemDescription\}\}/g, item.description);
        $('.list__wrapper').append(template);
      },
      showModal: function(){
        $('html, body').addClass('modal-is-open');
        $('.modal__wrapper').fadeIn('fast');
      },
      closeModal: function(){
        $('html, body').removeClass('modal-is-open');
        $('.modal__wrapper').fadeOut('fast');
      },
    };

    app.init();

    //Open form
    $('#newItem').on('click', function(e){
      e.preventDefault();
      app.showModal();
    });
    //Close form
    $('.modal__body--cancel').on('click', function(e){
      e.preventDefault();
      app.closeModal();
    });

    //Submit form
    $('.modal__body').on('submit', 'form', function(e){
      e.preventDefault();
      app.submitForm(this);
    });






})(jQuery, window, document);
