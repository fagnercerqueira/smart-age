/**
 * The module dependencies.
 */
const fs = require('fs');
const url = require('url');
const path = require('path');
const utils = require('./utils');

/**
 * Prepare the configuration.
 */
const config = {
	host: 'localhost',
	port: 3000,
	open: 'external',
	files: [
		utils.buildPath('**/*.css'),
		utils.buildPath('**/*.js'),
		utils.buildPath('**/*.html')
	],
	ghostMode: {
		clicks: false,
		scroll: true,
		forms: {
			submit: true,
			inputs: true,
			toggles: true
		}
	},
	snippetOptions: {
		rule: {
			match: /<\/body>/i,
			fn: (snippet, match) => `${snippet}${match}`
		}
	},
	server: {
		baseDir: utils.buildPath(),
		directory: true
	},
	reloadThrottle: 100
};

/**
 * Load the proxy configuration from `drone.json`.
 */
const dronePath = path.resolve(__dirname, '../drone.json');

if (fs.existsSync(dronePath)) {
	const { home_url } = JSON.parse(
		fs.readFileSync(dronePath, { encoding: 'utf-8' })
	);

	config.host = url.parse(home_url).hostname;
	config.proxy = home_url;

	delete config.server;
}

module.exports = config;
